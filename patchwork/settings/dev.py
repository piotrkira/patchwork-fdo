# Partial development settings for Patchwork, also used for testing.
# You want to use one of the database-specific configs instead.
from .base import *  # noqa

# Debugging
DEBUG = True

# Security
SECRET_KEY = '00000000000000000000000000000000000000000000000000'
INTERNAL_IPS = ['127.0.0.1', '::1']

# Email
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# django-debug-toolbar
INSTALLED_APPS += ['debug_toolbar']
DEBUG_TOOLBAR_PATCH_SETTINGS = False

MIDDLEWARE = [
    # Must be the first middleware in the list:
    'debug_toolbar.middleware.DebugToolbarMiddleware',
] + MIDDLEWARE

# Patchwork settings
ENABLE_XMLRPC = True
